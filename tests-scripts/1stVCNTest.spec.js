// @ts-check

const { chromium } = require('@playwright/test');

// Define a test case
test('example test', async ({ page }) => {
    const browser = await chromium.launch();
    const page = await browser.newPage();

    await page.goto('https://www.vcn.ca');
    await page.click('text=Services');

    await browser.close();
});



