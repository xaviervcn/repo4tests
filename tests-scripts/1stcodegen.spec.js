// @ts-check

import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://wordpress2.vcn.bc.ca/wp-signup.php?new=www.vcn.ca');
  await page.goto('https://www.cnn.com/');
  await page.locator('#pageHeader').getByRole('link', { name: 'Travel' }).click();
  await page.getByRole('link', { name: 'Stay', exact: true }).click();
  await page.getByRole('link', { name: 'Video', exact: true }).click();
});